#ifndef __DRV_DAC8552_H
#define __DRV_DAC8552_H


#include "main.h"


// channel
enum DAC8552_channel
{
  DAC8552_chA = 0,
  DAC8552_chB = 1
};

void DAC8552_Init();
void DAC8552_setChanValue(uint32_t ch_AorB, uint16_t chValue);
uint16_t DAC8552_conv(int32_t Vout_mv);


#endif /* __DRV_DAC8552_H */